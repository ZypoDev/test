<?php
    session_start();
    require_once('inc/SQL_users.php');
    $ban_ip = $bdd->prepare('SELECT * FROM ip WHERE user_ip = :user_ip');
    $ban_ip -> bindParam(':user_ip', $_SERVER['REMOTE_ADDR']);
    $ban_ip -> execute();
    $step_ban = $ban_ip->fetch(PDO::FETCH_OBJ);
    if($ban_ip->rowCount() >= 1) {
        if($step_ban->ban_ip == 1) {
            header("location: Error/ban.php");
    }   }
    if(isset($_POST['Ask_R']) && !empty($_POST['Ask_R']) && ($_SESSION['step'] == 4)) {
        $login4 = $bdd->prepare('SELECT * FROM user WHERE pseudo = :uid');
        $login4 -> bindParam(':uid', $_SESSION['UID']);
        $login4 -> execute();
        $result4 = $login4->fetch(PDO::FETCH_OBJ);
        if($login4->rowCount() >= 1) {
            if($result4->Ask_R == $_POST['Ask_R']) {
                $_SESSION['ETAT'] = TRUE;
                $_SESSION['step'] = 5;
    }   }   }
    if(isset($_POST['DAY']) && isset($_POST['MONTH']) && isset($_POST['YEAR']) && !empty($_POST['DAY']) && !empty($_POST['MONTH']) && !empty($_POST['YEAR']) && ($_SESSION['step'] == 3)) {
        if(($_POST['DAY'] && $_POST['MONTH'] && $_POST['YEAR']) != 0) {
            $login3 = $bdd->prepare('SELECT * FROM user WHERE pseudo = :uid');
            $login3 -> bindParam(':uid', $_SESSION['UID']);
            $login3 -> execute();
            $result3 = $login3->fetch(PDO::FETCH_OBJ);
            if($login3->rowCount() >= 1) {
                if($result3->day == $_POST['DAY']) {
                    if($result3->month == $_POST['MONTH']) {
                        if($result3->year == $_POST['YEAR']) {
                            $_SESSION['step'] = 4;
    }   }   }   }   }   }
    if(isset($_POST['PSSWD']) && !empty($_POST['PSSWD']) && ($_SESSION['step'] == 2)) {
        $login2 = $bdd->prepare('SELECT * FROM user WHERE pseudo = :uid');
        $login2 -> bindParam(':uid', $_SESSION['UID']);
        $login2 -> execute();
        $result2 = $login2->fetch(PDO::FETCH_OBJ);
        if($login2->rowCount() >= 1) {
            if($result2->password == md5($_POST['PSSWD'])) {
                $login_ip = $bdd->prepare('SELECT * FROM ip WHERE user_ip = :user_ip');
                $login_ip -> bindParam(':user_ip', $_SERVER['REMOTE_ADDR']);
                $login_ip -> execute();
                while($step_ip = $login_ip->fetch(PDO::FETCH_OBJ)) {
                    if($_SESSION['UID_ID'] == $step_ip->user_id) {
                        $_SESSION['ETAT'] = TRUE;
                        $_SESSION['step'] = 5;
                }   }
                if($_SESSION['step'] != 5) {
                    $_SESSION['step'] = 3;
    }   }   }   }
    if(isset($_POST['UID']) && !empty($_POST['UID']) && ($_SESSION['step'] == 1)) {
        $login1 = $bdd->prepare('SELECT * FROM user WHERE pseudo = :uid');
        $login1 -> bindParam(':uid', $_POST['UID']);
        $login1 -> execute();
        $result1 = $login1->fetch(PDO::FETCH_OBJ);
        if($login1->rowCount() >= 1) {
            if(($result1->pseudo == $_POST['UID']) && ($result1->ban != 1)) {
                $_SESSION['avatar'] = $result1->avatar;
                $_SESSION['UID_ID'] = $result1->id;
                $_SESSION['UID'] = $_POST['UID'];
                $_SESSION['step'] = 2;
            } else if($result1->ban == 1) {
                header("location: Error/ban.php");
    }   }   }
    if($_SESSION['step'] == 5) {
        if(isset($_SESSION['UID']) && !empty($_SESSION['UID'])) {
            if(isset($_SESSION['ETAT']) && !empty($_SESSION['ETAT']) && ($_SESSION['ETAT'] == TRUE)) {
                $login5 = $bdd->prepare('SELECT * FROM user WHERE pseudo = :uid');
                $login5 -> bindParam(':uid', $_SESSION['UID']);
                $login5 -> execute();
                $result5 = $login5->fetch(PDO::FETCH_OBJ);
                if($login5->rowCount() >= 1) {
                    $_SESSION['step'] = 6;
                    // - - - - - - - - - - - - - - - - - - - - - - - - - - //
                    $_SESSION['UID_ID'] = $result5->id;
                    $_SESSION['USER'] = $result5->pseudo;
                    $_SESSION['EMAIL'] = $result5->email;
                    // PASSWORD
                    // QUESTION SECRETTE
                    // REPONSE SECRETTE
                    $_SESSION['NAME'] = $result5->name;
                    $_SESSION['LAST_NAME'] = $result5->last_name;
                    $_SESSION['DAY'] = $result5->day;
                    $_SESSION['MONTH'] = $result5->month;
                    $_SESSION['YEAR'] = $result5->year;
                    $_SESSION['STAR'] = $result5->star;
                    $_SESSION['VOTE'] = $result5->vote;
                    $_SESSION['AVATAR'] = $result5->avatar;
                    $_SESSION['DESCRIPTION'] = $result5->description;
                    $_SESSION['CODE_LANGUAGE'] = $result5->code_language;
                    $_SESSION['SOFTWARE'] = $result5->software;
                    $_SESSION['PROJECT'] = $result5->project;
                    $_SESSION['GRADE'] = $result5->grade;
                    $_SESSION['SKYPE'] = $result5->skype;
                    $_SESSION['SKYPE_STATUT'] = $result5->skype_statut;
                    $_SESSION['TWITTER'] = $result5->twitter;
                    $_SESSION['WEB_SITE'] = $result5->web_site;
                    $_SESSION['SPIGOT'] = $result5->spigot_name;
                    $_SESSION['ALERT'] = $result5->alert;
                    $_SESSION['BAN'] = $result5->ban;
                    // - - - - - - - - - - - - - - - - - - - - - - - - - - //
                    $login_ip = $bdd->prepare('SELECT * FROM ip WHERE user_ip = :user_ip');
                    $login_ip -> bindParam(':user_ip', $_SERVER['REMOTE_ADDR']);
                    $login_ip -> execute();
                    $step_ip = $login_ip->fetch(PDO::FETCH_OBJ);
                    if(($login_ip->rowCount() >= 0) && ($step_ip->user_id != $_SESSION['UID_ID'])) {
                        $ip_add = $bdd->prepare('INSERT INTO ip(user_id, user_ip, ban_ip, alert_ip) VALUES (:user_id, :user_ip, :ban_ip, :alert_ip)');
                        $ip_add -> bindParam(':user_id', $_SESSION['UID_ID']);
                        $ip_add -> bindParam(':user_ip', $_SERVER['REMOTE_ADDR']);
                        $ip_add -> bindParam(':ban_ip', $_SESSION['BAN']);
                        $ip_add -> bindParam(':alert_ip', $_SESSION['ALERT']);
                        $ip_add -> execute(); }
                    header('location: profile.php');
    }   }   }   }
    if($_SESSION['step'] == 6) {
        header('location: profile.php');
    }
    if(!isset($_SESSION['step'])) {
        $_SESSION['step'] = 1;
    }
?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <meta name="robots" content="LaRuche, TheHive, Developpeur, Developpeurs, HiveDev, iDev, Réseaux, Social, Network, Commande">
        <meta name="author" content="Zypo_Dev">
        
        <link href="/css/style.css" rel="stylesheet">
        <link href="/css/style-responsive.css" rel="stylesheet">

        <title>Login | HiveDev.fr</title>
    </head>

    <body class="login-body">
        <div class="container">
            <?php
                if($_SESSION['step'] == 1) {
                    ?>
                    <!-- STEP 1 START -->
                    <form class="form-signin login-form" action="login.php" method="POST">
                        <div class="form-signin-heading text-center">
                            <h1 class="sign-title">Login - étape <?= $_SESSION['step']; ?></h1>
                            <img src="images/logo.png" alt=""/>
                            <div class="registration">
                                <img class= "sign-logo" src="<?= $_SESSION['avatar']; ?>" alt=""/>
                                <h4><p><center>Entrer un nom d'utilisateur</center></p></h4>
                            </div>
                        </div>
                        <div class="login-wrap">

                            <input class="form-control" type="text" name="UID" value="" placeholder="User ID" autofocus>
                            <button class="btn btn-lg btn-login btn-block" type="submit" name="submit"><i class="fa fa-check"></i></button>

                            <div class="registration">
                                Pas encore membre ?
                                <a class="" href="">
                                    S'enregistrer
                                </a>
                            </div>
                        </div>
                    </form>
                    <!-- SET 1 END -->
                    <?php
                } else if ($_SESSION['step'] == 2) {
                    ?>
                    <!-- STEP 4 START -->
                    <form class="form-signin login-form" action="login.php" method="POST">
                        <div class="form-signin-heading text-center">
                            <h1 class="sign-title">Login - étape <?= $_SESSION['step']; ?></h1>
                            <img src="images/logo.png" alt=""/>
                            <div class="registration">
                                <img class= "sign-logo" src="<?= $_SESSION['avatar']; ?>" alt=""/>
                                <h4><p><center><?= $_SESSION['UID']; ?></center></p></h4>
                            </div>
                        </div>
                        <div class="login-wrap">

                            <input class="form-control" type="password" name="PSSWD" value="" placeholder="Password" autofocus>
                            <button class="btn btn-lg btn-login btn-block" type="submit" name="submit"><i class="fa fa-check"></i></button>

                            <div class="registration">
                                Pas encore membre ?
                                <a class="" href="">
                                    S'enregistrer
                                </a>
                            </div>
                        </div>
                    </form>
                    <!-- SET 4 END-->
                    <?php
                } else if ($_SESSION['step'] == 3) {
                    ?>
                    <!-- STEP 1 : Vérification de niveaux 1 -->
                    <form class="form-signin login-form" action="login.php" method="POST">
                        <div class="form-signin-heading text-center">
                            <h1 class="sign-title">Login - étape <?= $_SESSION['step']; ?></h1>
                            <img src="images/logo.png" alt=""/>
                            <div class="registration">
                                <img class= "sign-logo" src="<?= $_SESSION['avatar']; ?>" alt=""/>
                                <h4><p><center><?= $_SESSION['UID']; ?></center></p></h4>
                            </div>
                        </div>
                        <div class="login-wrap">
                            <div class="col-md-4">
                                <select class="form-select" name="DAY">
                                    <option value="0">day</option>
                                    <?php
                                        $i = 1;
                                        while ($i <= 31) {
                                            echo '<option value="'.$i.'">'.$i.'</option>';
                                            $i++;
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-4">
                                <select class="form-select" name="MONTH">
                                    <option value="0">month</option>
                                    <?php
                                        $i = 1;
                                        while ($i <= 12) {
                                            echo '<option value="'.$i.'">'.$i.'</option>';
                                            $i++;
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-4">
                                <select class="form-select" name="YEAR">
                                    <option value="0">year</option>
                                    <?php
                                        $i = 2000;
                                        while ($i >= 1900) {
                                            echo '<option value="'.$i.'">'.$i.'</option>';
                                            $i--;
                                        }
                                    ?>
                                </select>
                            </div>
                            <button class="btn btn-lg btn-login btn-block" type="submit" name="submit"><i class="fa fa-check"></i></button>

                            <div class="registration">
                                Pas encore membre ?
                                <a class="" href="">
                                    S'enregistrer
                                </a>
                            </div>
                        </div>
                    </form>
                    <!-- SET 1 end -->
                    <?php
                } else if ($_SESSION['step'] == 4) {
                    $login4_1 = $bdd->prepare('SELECT * FROM user WHERE pseudo = :uid');
                    $login4_1 -> bindParam(':uid', $_SESSION['UID']);
                    $login4_1 -> execute();
                    $result4_1 = $login4_1->fetch(PDO::FETCH_OBJ);
                    ?>
                    <!-- STEP 2 START -->
                    <form class="form-signin login-form" action="login.php" method="POST">
                        <div class="form-signin-heading text-center">
                            <h1 class="sign-title">Login - étape <?= $_SESSION['step']; ?></h1>
                            <img src="images/logo.png" alt=""/>
                            <div class="registration">
                                <img class= "sign-logo" src="<?= $_SESSION['avatar']; ?>" alt=""/>
                                <h4><p><center><?= $_SESSION['UID']; ?></center></p></h4>
                            </div>
                        </div>
                        <div class="login-wrap">
                            <div class="col-md-7">
                                <input class="form-control" type="text" name="Ask_Q" value="<?= $result4_1->Ask_Q; ?>" disable>
                            </div>
                            <div class="col-md-5">
                                <input class="form-control" type="text" name="Ask_R" value="" placeholder="Réponse">
                            </div>
                            <button class="btn btn-lg btn-login btn-block" type="submit" name="submit"><i class="fa fa-check"></i></button>

                            <div class="registration">
                                Pas encore membre ?
                                <a class="" href="">
                                    S'enregistrer
                                </a>
                            </div>
                        </div>
                    </form>
                    <!-- SET 4 END -->
                    <?php
                }
            ?>
        </div>
    </body>
    <footer class="sticky-footer">
        <?php include_once('inc/footer.php'); ?>
    </footer>
    <foot>
        <script src="js/jquery-1.10.2.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/modernizr.min.js"></script>
    </foot>
</html>