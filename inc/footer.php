<?= date("Y"); ?> &copy; <a href="">HiveDev.fr </a>| votre ip : <?= $_SERVER['REMOTE_ADDR']; ?>
<?php
    require_once('inc/SQL_users.php');
    if(!isset($_SESSION['UID_ID'])) { $_SESSION['UID_ID'] = 0; }
    $ip_history = $bdd->prepare('INSERT INTO history(ip, user_id, page_path) VALUES (:ip, :user_id, :page_path)');
    $ip_history -> bindParam(':ip', $_SERVER['REMOTE_ADDR']);
    $ip_history -> bindParam(':user_id', $_SESSION['UID_ID']);
    $ip_history -> bindParam(':page_path', $_SERVER['PHP_SELF']);
    $ip_history -> execute();
?>